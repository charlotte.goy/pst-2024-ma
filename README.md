# PST_MA

## Main contributors

The main contributors (in alphabetic order) for this project are
 - Flavie CHARPENTIER
 - Charlotte GOY
 - Manon TUBIANA

## remarques:

verifier dans le fichier iterator.py que les valeurs aux lignes 73 et 75 sont correctes (i.e. colonne 2 = micro et colonne 1 = photo). 

est-ce que votre micro a sature?

quand vous serez amene a changer le seuil (qui vaut 0.00176498), il faut le changer le partout !

interpreter/critiquer la precision finale
##

The goal of this project is to predict the behavior of the flame.
- [ ] first version: predict in the near future a binary value

## Coming up soon

- [x] set-up gitlab account
- [x] add your data reader
- [x] (06/02/2023 10h30) we saw that our data contain 299,999 lines, each with 9 elements. Each of these elements correspond to: time (0.1ms), 4 micro (V), flame intensity, 3 useless values
- [x] (06/02/2023 10h30) (for ey) fix the progress bar
- [ ] check this [tutorial](https://www.tensorflow.org/tutorials/keras/classification)
- [x] (06/02/2023 10h30) define a label (the state of the flame): based on the variance from $t$ to $t+n$ and the inputs are collected from $t-n$ to $t$.
- [ ] (todo) create a data iterator (input X and label Y) -> define the "future"
- [ ] create the model (wait for the above)



## How to setup

You will need a few things. First, cloen this repository, using something like `git clone git@gitlab.com:ey_datakalab/pst_ma.git`. 

## How to install

Once you went through the set-up, you can install the required python packages. In a conda environment, you can simply run `python -m src.plane_motor` and it will give you the pip commands to use. Follow these steps:
 1. run `conda create -n pst python=3.9`
 2. run `conda activate pst`
 3. run `python -m src.plane_motor` and follow the instructions from there

Note that these instructions won't work on a M1/M2 mac check [this link](https://caffeinedev.medium.com/how-to-install-tensorflow-on-m1-mac-8e9b91d93706).

## tips:

Descent gradient

$$F_\theta : (X) \rightarrow F(X) \sim Y$$

Par exemple:
    Si $F$ est un réseau de neurones avec $F : W_2 ReLU(W_1 X)$, ici $\theta = (W_1, W_2)$

$\theta$ définit la fonction $F$.
$\theta$ = initialisé de manière aléatoire

boucle jusqu'à convervgence:
 1. on prend des exemples au hasard $X,Y$ (limité par les ressources de calcul)
 2. on fait une prédiction : $F(X) \neq Y \rightarrow$ distance entre $F(X)$ et $Y$ est l'erreur de prédiction
 3. on calcule les gradients de \theta par rapport à l'erreur et on met à jour \theta:
        $\theta = \theta - grad_\theta \times lr $
    où $lr<<1$ désigne le learning rate.
