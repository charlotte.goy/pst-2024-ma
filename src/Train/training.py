import matplotlib.pyplot as plt
import tensorflow as tf

from .model import create_model

print(create_model)


def train_a_model(training_set: tf.data.Dataset, example_duration: int) -> None:
    """
    trains a neural network

    Args:
        training_set: data iterator
        ???
    """
    model = create_model(example_duration=example_duration)  # to complete
    loss_function = tf.keras.losses.BinaryCrossentropy(
        from_logits=False
    )  # pk la cross entropy et pas la précision directement?
    optimizer = tf.keras.optimizers.Adam(learning_rate=0.00001)
    model.compile(metrics=["accuracy"], loss=loss_function)  # uncomment this after you have defined your model

    @tf.function
    def optimization_step(images: tf.Tensor, labels: tf.Tensor) -> tf.Tensor:
        """
        runs an optimization step

        Args:
            images: inputs to use
            labels: ground truth
        """
        with tf.GradientTape() as tape:
            tape.watch(model.trainable_variables)
            predictions = model(images, training=False)
            loss = loss_function(y_true=labels, y_pred=predictions)
            gradients = tape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(gradients, model.trainable_variables))
        return loss, predictions

    losses = []
    for cpt, (inputs, labels) in enumerate(training_set):
        print(f"\r{100 * cpt / 1000:.3f}%", end="")
        loss_value, preds = optimization_step(
            images=inputs, labels=labels
        )  # uncomment this after you have compelted the rest of the code
        losses.append(loss_value.numpy())
        if cpt == 1000:
            break
    print("\r100.000%")
    plt.plot(list(range(len(losses))), losses)
    plt.show()
    model.save("trained_model.h5")
