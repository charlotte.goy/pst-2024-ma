import os

import numpy as np


def load_data_v2(folder_path: str) -> np.ndarray:
    if folder_path == "":
        folder_path = input("Name of the folder:")
    all_files = [os.path.join(folder_path, element) for element in os.listdir(folder_path) if ".lvm" in element]
    all_data = []
    for path_to_file in all_files:
        with open(path_to_file, "r") as f:
            data = f.read().splitlines()
        start_reading_lines = False
        all_values = []
        for row in data:
            if start_reading_lines and len(row.split("\t")) == 3:
                all_values.append([float(e.replace(",", ".")) for e in row.split("\t")])
            if row.split("\t")[0] == "X_Value":
                start_reading_lines = True
        all_data.append(all_values)
    reference_length = min([len(data_block) for data_block in all_data])
    for cpt in range(len(all_data)):
        all_data[cpt] = all_data[cpt][:reference_length]
    return np.array(all_data)  # dimensions: [nombre-de-fichiers x reference_lengths x 3]
