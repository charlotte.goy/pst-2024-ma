import os
from typing import List

import numpy as np


def load_data(folder_path: str = "") -> List[np.ndarray]:
    """
    This function loads the data from a folder. The output is a list
    of numpy arrays. Each numpy array corresponds to the data a file.
    Each data file contains a list of measures (one row = one sample)

    Args:
        folder_path: path to the folder
    """
    if folder_path == "":
        folder_path = input("Name of the folder:")
    if "data.npy" in os.listdir(folder_path):
        return np.load(os.path.join(folder_path, "data.npy"))
    all_files = [os.path.join(folder_path, element) for element in os.listdir(folder_path) if ".txt" in element]
    output = []
    for file in all_files:
        output_per_file = []
        try:
            with open(file, "r") as f:
                datafile = f.read().splitlines()
            for cpt, line in enumerate(datafile):
                line = line.split()
                tmp = [float(e) for e in line]
                if len(tmp) != 0:
                    output_per_file.append(tmp)
                print(f"\r{100 * cpt / len(datafile):.2f}%", end="")
            output_per_file = np.array(output_per_file)
            output.append(output_per_file)
            print(f"\r{100:.2f}%")

        except OSError:
            print("Error in reading file {:s}. Closing program.".format(file))
    np.save(os.path.join(folder_path, "data.npy"), np.array(output))
    return output


if __name__ == "__main__":
    output = load_data(folder_path="data")[0]
    # np.save("data1.npy", output)
    stds = []
    for cpt in range(300):
        stds.append(np.std(output[cpt * 1000 : (cpt + 1) * 1000, 7]))
    print(np.percentile(stds, 95))
    print(np.percentile(stds, 90))
    print(np.percentile(stds, 80))
    print(np.percentile(stds, 50))
    print(np.percentile(stds, 25))
