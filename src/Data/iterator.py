from typing import Tuple

import numpy as np
import tensorflow as tf


def create_training_data_iterator(
    raw_data: np.ndarray,
    batch_size: int = 32,
    example_duration: int = 70,
    is_training: bool = True,
) -> tf.data.Dataset:
    """
    creates a data.Dataset for tensorflow training
    This behaves like list or a python iterator, i.e. you can use it in a for loop
    The advantage over a simpel list is its use of the gpu/cpu

    Args:
        raw_data: list of numpy arrays of raw data from which we will define
            our training examples
        batch_size: training btach size (size of a batch of examples for each training step)
        example_duration: duration (in terms of array length) of one example ??? le nombre de données par batch
    """
    # we will use tf.data.Dataset.from_tensor_slices
    # this function will take a list/tuple of lists
    # for example [0,1,2,3,4] and when we will iterate
    # over the dataset, it will return batches such as
    # [0,2] then [3,1] then [4,1]...
    # so we need to define a proper way to iterate over our raw data

    # because we will havem ultiple arrays that are not compatible (we do not want
    # to mix different files) we will give two values: the file number and the time stamp

    slices = (
        [
            cpt
            for cpt in range(len(raw_data))  # index of rows --> expérience ==numéro du fichier
            for _ in range(
                len(raw_data[cpt]) - int(2 * example_duration)
            )  # == numéro de la ligne de départ  indix in time -->calculate the number of rows or columns possible to include in the table of length 2
        ],
        [cpt for cpt_ in range(len(raw_data)) for cpt in range(len(raw_data[cpt_]) - int(2 * example_duration))],
    )
    dataset = tf.data.Dataset.from_tensor_slices(slices)  # function is working with the argument slices (to ask)
    if is_training:
        dataset = dataset.shuffle(
            len(slices[0])
        )  # je réordonne les fichiers aéatoirement une fois que j'ai fait une bouclerandomly pick a dataset from the function above, which length is equal to the length of slices 0 her (but can be 1 or 2 or 165)
        dataset = dataset.repeat()  # liste de longueur infinie repeat the operation

    threshold = 0.00176498  # 80ème percentile

    tf_raw_data = tf.constant(
        value=raw_data
    )  # creatioon of a tensor à 3 dimensions nomb d'exp x nombre lignes qui vont varier slonle fichier x nombre de colonnes

    @tf.function
    def load_batch(  # 32 paire y un nombre les 4 colonnes et ??
        exp_idx: tf.Tensor,
        # première liste de slices index de l'exp et deuxième liste de slices index de la ligne
        time_stamp: tf.Tensor,
    ) -> Tuple[tf.Tensor, tf.Tensor]:  # on sort un tuple avec le sinpus et les laels toujours
        # en tensorfloq notre np array
        exp_data = tf.gather(tf_raw_data, exp_idx)
        x_time_stamps = (
            tf.constant(value=list(range(example_duration))) + time_stamp
        )  # tenseur de longueur ex duration + notre indice
        y_time_stamps = tf.constant(value=list(range(example_duration))) + time_stamp + example_duration

        x = tf.gather(exp_data, x_time_stamps)  # will be the inputs (represents data from the past)
        y = tf.gather(exp_data, y_time_stamps)  # will be the labels (represents a label based on the future)
        x = tf.gather(
            x, tf.constant(2), axis=-1
        )  # matrice tensor à 2 dim autant de ligne que example duration et avec 4 lignes
        y = tf.gather(y, tf.constant(1), axis=-1)  # une seule colonne et autant de ligne que example duration

        # do the transformation of y here
        # on veut untruc qui n'a qu'une seule valeur sur un truc qui a mille lignes
        # si la variance est stable ok si la variance varie pas ok
        # reduce std ou reduce bar
        # il faut trouver un moyen de transformer ça en disant plus petite ou plus grans
        # tf cond est à utiliser

        variance = tf.math.reduce_std(y)

        y = tf.cast(tf.greater(variance, threshold), dtype=tf.float32)

        return x, y

    dataset = dataset.map(load_batch, num_parallel_calls=tf.data.AUTOTUNE, deterministic=False)
    dataset = dataset.batch(batch_size)
    return dataset
